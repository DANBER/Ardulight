# Ardulight

With this script you can control a LED-Stripe with your Arduino Board. You can use your old IR-Controller. There is also a sunrise simulation.

## Images

![](images/arduino1.jpg)
![](images/arduino2.jpg)
![](images/light.png)