#include "IRremote.h"

#define REDPIN 5
#define GREENPIN 6
#define BLUEPIN 9
#define IRPIN 11
#define MUSICPIN 0

void strobo(int red, int green, int blue);
void jump();
void fade ();
void zufall();
void sunrise(float dauer);

void setColor(int red, int green, int blue);
void testResult();
void setMode();

// ================================================================================================

IRrecv reciever(IRPIN);
decode_results results;

int mode = 0;
int modeOld = 0;
unsigned long result = 0;
unsigned long resultOld = 0;
int flag = 0;
int pause = 50;
int light = 255;
long currentMillis = 0;
long previousMillis = 0;

void setup()
{
  Serial.begin(9600);

  pinMode(REDPIN, OUTPUT);
  pinMode(GREENPIN, OUTPUT);
  pinMode(BLUEPIN, OUTPUT);

  reciever.enableIRIn();
}

void loop()
{
  testResult();
  setMode();
}

// ================================================================================================

void setColor(int red, int green, int blue)
{
  analogWrite(REDPIN, red);
  analogWrite(GREENPIN, green);
  analogWrite(BLUEPIN, blue);
}

// ================================================================================================

// White strobo light
void strobo(int red, int green, int blue)
{
  currentMillis = millis();
  if (currentMillis - previousMillis > pause && flag == 0) {
    previousMillis = currentMillis;
    flag = 1;
    setColor(red * light / 255, green * light / 255, blue * light / 255);
  }
  if (currentMillis - previousMillis > pause && flag == 1) {
    previousMillis = currentMillis;
    flag = 0;
    setColor(0, 0, 0);
  }
}

// ================================================================================================

// Jump between RGB colors
void jump()
{
  currentMillis = millis();
  if (currentMillis - previousMillis > pause && flag == 0) {
    previousMillis = currentMillis;
    flag = 1;
    setColor(light, 0, 0);
  }
  if (currentMillis - previousMillis > pause && flag == 1) {
    previousMillis = currentMillis;
    flag = 2;
    setColor(0, light, 0);
  }
  if (currentMillis - previousMillis > pause && flag == 2) {
    previousMillis = currentMillis;
    flag = 0;
    setColor(0, 0, light);
  }
}

// ================================================================================================

// Fade through different HUE colors
void fade ()
{
  currentMillis = millis();
  static int i = 0;
  if (flag == 0) {
    flag = 1;
    i = 0;
    pause = 100;
  }
  if (currentMillis - previousMillis > pause && flag == 1) {
    previousMillis = currentMillis;
    i = i + 1;

    float h, s, v, f, p, q, t;

    h = (i % 360) / 60.0;
    s = 1.0;
    v = 1.0;
    f = h - floor(h); // factorial part of h
    p = v * (1 - s);
    q = v * (1 - s * f);
    t = v * (1 - s * (1 - f));

    switch ((int)h) {
      case 0:
        setColor(255 * v, 255 * t, 255 * p);
        break;
      case 1:
        setColor(255 * q, 255 * v, 255 * p);
        break;
      case 2:
        setColor(255 * p, 255 * v, 255 * t);
        break;
      case 3:
        setColor(255 * p, 255 * q, 255 * v);
        break;
      case 4:
        setColor(255 * t, 255 * p, 255 * v);
        break;
      case 5:
        setColor(255 * v, 255 * p, 255 * q);
    }
  }
}

// ================================================================================================

// Select one random color
void zufall()
{
  currentMillis = millis();
  if (currentMillis - previousMillis > pause && flag == 0) {
    previousMillis = currentMillis;
    flag = 1;
    setColor((int)random(light), (int)random(light), (int)random(light));
  }
}

// ================================================================================================

// Make a sunrise simulation
void sunrise(float dauer)
{
  static float i = 0;
  currentMillis = millis();
  if (flag == 0) {
    flag = 1;
    i = 0;
    setColor(0, 0, 0);
  }

  if (flag == 1) {
    if (currentMillis - previousMillis > 50) {
      previousMillis = currentMillis;
      i = i + 350 / dauer;

      int red, green, blue;

      if ( i / 100 <= 66 ) {
        // http://www.tannerhelland.com/4435/convert-temperature-rgb-algorithm-code/
        red = 329.698727446 * (pow(i / 100.0, -0.1332047592));
        green = 99.4708025861 * log(i / 100) / log(2.7182818284) - 161.1195681661;
        blue = 138.5177312231 * log(i / 100 - 10) / log(2.7182818284) - 305.0447927307;
      }
      else {
        red = green = blue = 255;
      }

      setColor(red < 0 ? 0 : (red > 255 ? 255 : red), green < 0 ? 0 : (green > 255 ? 255 : green), blue < 0 ? 0 : (blue > 255 ? 255 : blue));
    }
  }
}

// ================================================================================================

void testResult()
{
  if (reciever.decode(&results)) {
    result = results.value;

    if (modeOld != mode) {
      previousMillis = currentMillis - pause - 1;
      flag = 0;
    }
    modeOld = mode;

    if (resultOld != result) {
      if (result != 4294967295) {
        resultOld = result;
      }
    }
    Serial.println(result, DEC);
    Serial.println(resultOld, DEC);

    switch (result) {
      case 16712445:
        //Aus
        mode = 0;
        break;

      case 4294967295:
        //pressed
        if (resultOld == 16726725) {
          if (light <= 250) {
            light = light + 5;
          }
        }
        if (resultOld == 16759365) {
          if (light >= 5) {
            light = light - 5;
          }
        }
        if (resultOld == 16771095) {
          if (pause >= 5) {
            pause = pause - 5;
          }
        }
        if (resultOld == 16762935) {
          if (pause <= 250) {
            pause = pause + 5;
          }
        }
        break;

      case 16720605:
        //white
        mode = 6;
        break;

      case 16718565:
        //red
        mode = 7;
        break;

      case 16751205:
        //green
        mode = 8;
        break;

      case 16753245:
        //blue
        mode = 9;
        break;

      case 16726725:
        //hell
        if (light < 255) {
          light = light + 5;
        }
        break;

      case 16759365:
        //dunkel
        if (light >= 5) {
          light = light - 5;
        }
        break;

      case 16771095:
        //schnell
        if (pause >= 5) {
          pause = pause - 5;
        }
        break;

      case 16762935:
        //langsam
        if (pause < 3000) {
          pause = pause + 5;
        }
        break;

      case 16736415:
        //fade3
        mode = 3;
        break;

      case 16720095:
        //jump3
        mode = 4;
        break;

      case 16764975:
        //flash
        mode = 10;
        break;

      case 16724175:
        //diy1
        break;

      case 16756815:
        //diy2
        break;

      case 16740495:
        //diy3
        mode = 5;
        break;

      case 16716015:
        //diy4
        mode = 11;
        break;

      case 16748655:
        //diy5
        break;

      case 16732335:
        //diy6
        break;

      case 16745085:
        //play
        break;

      case 16722645:
        //(1|1)
        mode = 12;
        break;

      case 16755285:
        //(1|2)
        mode = 13;
        break;

      case 16749165:
        //(1|3)
        mode = 14;
        break;

      case 16716525:
        //(1|4)
        mode = 15;
        break;

      case 16714485:
        //(2|1)
        mode = 16;
        break;

      case 16747125:
        //(2|2)
        mode = 17;
        break;

      case 16757325:
        //(2|3)
        mode = 18;
        break;

      case 16724685:
        //(2|4)
        mode = 19;
        break;

      case 16726215:
        //(3|1)
        mode = 20;
        break;

      case 16758855:
        //(3|2)
        mode = 21;
        break;

      case 16742535:
        //(3|3)
        mode = 22;
        break;

      case 16775175:
        //(3|4)
        mode = 23;
        break;

      case 16718055:
        //(4|1)
        mode = 24;
        break;

      case 16750695:
        //(4|2)
        mode = 25;
        break;

      case 16734375:
        //(4|3)
        mode = 26;
        break;

      case 16767015:
        //(4|4)
        mode = 27;
        break;

      case 16752735:
        //jump7
        break;

      case 16769055:
        //fade7
        break;

      case 16773135:
        //auto
        break;

      case 16722135:
        //redup
        break;

      case 16713975:
        //reddown
        break;

      case 16754775:
        //greenup
        break;

      case 16746615:
        //greendown
        break;

      case 16738455:
        //blueup
        break;

      case 16730295:
        //bluedown
        break;
    }

    reciever.resume();
  }
}

// ================================================================================================

void setMode()
{
  switch (mode) {
    case 0:
      setColor(0, 0, 0);
      break;

    case 3:
      fade();
      break;

    case 4:
      jump();
      break;

    case 5:
      zufall();
      break;

    case 6:
      setColor(light, light, light);
      break;

    case 7:
      setColor(light, 0, 0);
      break;

    case 8:
      setColor(0, light, 0);
      break;

    case 9:
      setColor(0, 0, light);
      break;

    case 10:
      strobo(255, 255, 255);
      break;

    case 11:
      sunrise(20);
      break;

    case 12:
      setColor(255 * light / 255, 60 * light / 255, 0 * light / 255);
      break;

    case 13:
      setColor(170 * light / 255, 255 * light / 255, 0 * light / 255);
      break;

    case 14:
      setColor(60 * light / 255, 0 * light / 255, 255 * light / 255);
      break;

    case 15:
      setColor(100 * light / 255, 100 * light / 255, 100 * light / 255);
      break;

    case 16:
      setColor(255 * light / 255, 110 * light / 255, 0 * light / 255);
      break;

    case 17:
      setColor(0 * light / 255, 255 * light / 255, 130 * light / 255);
      break;

    case 18:
      setColor(100 * light / 255, 0 * light / 255, 255 * light / 255);
      break;

    case 19:
      setColor(0 * light / 255, 0 * light / 255, 100 * light / 255);
      break;

    case 20:
      setColor(255 * light / 255, 150 * light / 255, 0 * light / 255);
      break;

    case 21:
      setColor(0 * light / 255, 255 * light / 255, 255 * light / 255);
      break;

    case 22:
      setColor(200 * light / 255, 0 * light / 255, 255 * light / 255);
      break;

    case 23:
      setColor(0 * light / 255, 100 * light / 255, 0 * light / 255);
      break;

    case 24:
      setColor(255 * light / 255, 225 * light / 255, 0 * light / 255);
      break;

    case 25:
      setColor(0 * light / 255, 130 * light / 255, 255 * light / 255);
      break;

    case 26:
      setColor(0 * light / 255, 255 * light / 255, 200 * light / 255);
      break;

    case 27:
      setColor(100 * light / 255, 0 * light / 255, 0 * light / 255);
      break;
  }
}
